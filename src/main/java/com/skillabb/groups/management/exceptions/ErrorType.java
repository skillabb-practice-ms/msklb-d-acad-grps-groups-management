package com.skillabb.groups.management.exceptions;


/**
 * Enum que define los tipos de error en la respuesta de la petición HTTP.
 * 
 * @author Jesus Cruz.
 */
public enum ErrorType {

  ERROR, WARN, INVALID, FATAL;

}

