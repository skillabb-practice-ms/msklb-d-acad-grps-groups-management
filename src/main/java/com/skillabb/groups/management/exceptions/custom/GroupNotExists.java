package com.skillabb.groups.management.exceptions.custom;

public class GroupNotExists extends RuntimeException {

  /**
   * 
   */
  private static final long serialVersionUID = 1L;

  public GroupNotExists(String message) {
    super(message);
  }

}
