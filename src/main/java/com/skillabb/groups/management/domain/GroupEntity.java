package com.skillabb.groups.management.domain;

import com.skillabb.groups.management.constants.EntityConstants;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;


/**
 * Entity que representa a la tabla groups en la db.
 * 
 * @author Jesus Cruz.
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@Entity
@Table(name = EntityConstants.TABLE_NAME)
public class GroupEntity {

  @Id
  @SequenceGenerator(name = EntityConstants.SEQUENCE_NAME,
      sequenceName = EntityConstants.SEQUENCE_NAME,
      allocationSize = EntityConstants.ALLOCATION_SIZE)
  @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = EntityConstants.SEQUENCE_NAME)
  private Integer id;

  private String name;

  private Integer status;

  private String abbreviation;

  private Integer status2;

  private String code;

  private String generation;

  private LocalDateTime createdAt;

  private LocalDateTime lastModifiedAt;
}
