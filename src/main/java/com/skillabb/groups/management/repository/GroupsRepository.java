package com.skillabb.groups.management.repository;

import com.skillabb.groups.management.domain.GroupEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface GroupsRepository extends JpaRepository<GroupEntity, Integer> {

  public List<GroupEntity> findGroupById(Integer id);

}
