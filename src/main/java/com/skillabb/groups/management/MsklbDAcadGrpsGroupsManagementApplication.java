package com.skillabb.groups.management;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;


@EnableEurekaClient
@SpringBootApplication
@ComponentScan({"com.skillabb*", "com.skillabb.*"})
public class MsklbDAcadGrpsGroupsManagementApplication {

  public static void main(String[] args) {
    SpringApplication.run(MsklbDAcadGrpsGroupsManagementApplication.class, args);
  }

}
