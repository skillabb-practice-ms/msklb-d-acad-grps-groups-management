package com.skillabb.groups.management.model;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@ApiModel
public class ReadGroupRequest {

  /**
   * Id del grupo.
   */
  @NotNull
  private Integer groupId;

}
