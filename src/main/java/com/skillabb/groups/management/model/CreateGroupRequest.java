package com.skillabb.groups.management.model;

import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
@ApiModel
public class CreateGroupRequest {

  @NotEmpty
  private String name;

  @NotNull
  private Integer status;

  @NotEmpty
  private String abbreviation;

  @NotNull
  private Integer status2;

  @NotEmpty
  private String code;

  @NotEmpty
  private String generation;
}
