package com.skillabb.groups.management.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Clase que contiene las constantes usadas por la entidad.
 * 
 * @author Jesus Cruz.
 */

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class EntityConstants {

  /**
   * Constante que tiene el nombre de la tabla.
   */
  public static final String TABLE_NAME = "Groups";

  /**
   * Constante que tiene el nombre de la secuencia.
   */
  public static final String SEQUENCE_NAME = "group_sequence";

  /**
   * Constante que tiene el incremento de la secuencia.
   */
  public static final int ALLOCATION_SIZE = 1;

  /**
   * Constante que tiene el status del grupo de inicio.
   */
  public static final int GROUP_STATUS = 1;

  /**
   * Constante que tiene el status 2 del grupo de inicio.
   */
  public static final int GROUP_STATUS_DOS = 1;

  /**
   * Constante que el nombre del grupo de inicio.
   */
  public static final String GROUP_NAME = "Group A";

  /**
   * Constante que el nombre abreviado del grupo de inicio.
   */
  public static final String GROUP_ABBREVIATION = "gprA";

  /**
   * Constante que el codigo del grupo de inicio.
   */
  public static final String GROUP_CODE = "00001";

  /**
   * Constante la generacion del grupo de inicio.
   */
  public static final String GROUP_GENERATION = "2022";

}
