package com.skillabb.groups.management.constants;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * Clase de constantes generales.
 * 
 * @author Jesus Cruz.
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Constants {

  /**
   * Constante que contiene el valor del base path.
   */
  public static final String BASE_PATH = "${api.uri.basePath}";

  /**
   * Constante que contiene el valor del specific path para el api de creacin de grupo.
   */
  public static final String GROUP_CREATE_SPECIFIC_PATH =
      "${api.uri.domain.context.mapping.create}";

  /**
   * Constante que contiene el valor del specific path para el api de eliminacion de grupo.
   */
  public static final String GROUP_DELETE_SPECIFIC_PATH =
      "${api.uri.domain.context.mapping.delete}";

  /**
   * Constante que contiene el valor del specific path para el api de lecutra de un grupo.
   */
  public static final String GROUP_READ_SPECIFIC_PATH = "${api.uri.domain.context.mapping.read}";

  /**
   * Constante que contiene el valor del specific path para el api de lecutra de un grupo.
   */
  public static final String GROUP_UPDATE_SPECIFIC_PATH =
      "${api.uri.domain.context.mapping.update}";

  /**
   * Constante utilizada para mostrar el status code 204.
   */
  public static final int CODE_NO_CONTENT = 204;
  /**
   * Constante utilizada para mostrar el status code 400.
   */
  public static final int CODE_BAD_REQUEST = 400;
  /**
   * Constante utilizada para mostrar el status code 401.
   */
  public static final int CODE_UNAUTHORIZED = 401;
  /**
   * Constante utilizada para mostrar el status code 403.
   */
  public static final int CODE_ACCESS_NOT_CONFIGURED = 403;
  /**
   * Constante utilizada para mostrar el status code 404.
   */
  public static final int CODE_RESOURCE_NOT_FOUND = 404;

  /**
   * Constante utilizada para mostrar el status code 500.
   */
  public static final int CODE_INTERNAL_ERROR = 500;

  /**
   * Consnate utilizada para mostrar el status code 422.
   */
  public static final int CODE_UNPROCESSABLE_ENTITY = 422;

  /**
   * Constante para mostrar Unprocessable Entity.
   */
  public static final String UNPROCESSABLE_ENTITY = "Unprocessable Entity";

  /**
   * Constante utilizada para mostrar el status code 408.
   */
  public static final int CODE_TIMEOUT = 408;

  /**
   * Constante utilizada para mostrar un mensaje acerca de recurso que no pudo ser encontrado.
   */
  public static final String RESOURCE_NOT_FOUND = "Resource not found";

  /**
   * Constante utilizada para mostrar un mensaje acerca de un Internal server error.
   */
  public static final String INTERNAL_ERROR = "Internal server error";

  /**
   * Constante utilizada para mostrar un mensaje acerca de una petición mal formada.
   */
  public static final String BAD_REQUEST = "Bad Request";

  /**
   * Constante utilizada para mostrar un mensaje acerca de una petición no autorizada.
   */
  public static final String UNAUTHORIZED = "Unauthorized";

  /**
   * Constante utilizada para mostrar un mensaje acerca de un error por time-out.
   */
  public static final String MESSAGE_TIMEOUT =
      "El servidor ha decidido cerrar la conexión en lugar de continuar esperando";

  /**
   * Constante que representa el header uuid.
   */
  public static final String UUID = "uuid";

  /**
   * Constante para indicar el header de swagger.
   */
  public static final String HEADER_WORD_CONSTANT = "header";

  /**
   * Constante que contiene el valor de la descripcion del api.
   */
  public static final String API_DESCRIPTION = "API que permite crear un grupo.";

  /**
   * Constante que contiene el valor de la descripcion del api.
   */
  public static final String API_DESCRIPTION_UPDATE = "API que permite actualizar un grupo.";

  /**
   * Constante que contiene el valor de la descripcion del api.
   */
  public static final String API_DESCRIPTION_DELETE = "API que permite eliminar un grupo.";

  /**
   * Constante que contiene el valor de la descripcion del api.
   */
  public static final String API_DESCRIPTION_READ = "API que permite consultar un grupo.";

  /**
   * Constante que representa el header Content-Type.
   */
  public static final String CONTENT_TYPE = "Content-Type";

  /**
   * Constante que representa el header Accept.
   */
  public static final String ACCEPT = "Accept";

  /**
   * Constante boolean false.
   */
  public static final boolean FALSE = false;

  /**
   * Constante boolean true.
   */
  public static final boolean TRUE = true;

  /**
   * Constante que contiene la cadena OK.
   */
  public static final String OK = "OK";

  /**
   * Constante que contiene el log para eliminar grupo.
   */
  public static final String START_DELET = "Inicia la eliminacion de grupo";

  /**
   * Constante que contiene el log para eliminar grupo.
   */
  public static final String START_CREATE = "Inicia la creacion de grupo";

  /**
   * Constante que contiene el log para eliminar grupo.
   */
  public static final String START_READ = "Inicia la lectura del grupo";

  /**
   * Constante que contiene el log para eliminar grupo.
   */
  public static final String START_UPDATE = "Inicia la lectura del grupo";

  /**
   * Constante que representa el tipo de error.
   */
  public static final String ERROR_TYPE = "type: {}";

  /**
   * Constante que representa el codigo de error.
   */
  public static final String ERROR_CODE = "code: {}";

  /**
   * Constante que representa el detalle del error.
   */
  public static final String ERROR_DETAILS = "details: {}";

  /**
   * Constante que representa la URI invocada.
   */
  public static final String ERROR_LOCATION = "location: {}";

  /**
   * Constante que representa informacion adicional del error.
   */
  public static final String ERROR_MORE_INFORMATION = "moreInfo: {}";

  /**
   * Constante que representa un mensaje.
   */
  public static final String MESSAGE_1 = "Group with: ";

  /**
   * Constante que representa un mensaje.
   */
  public static final String MESSAGE_2 = " does no exists";


}
