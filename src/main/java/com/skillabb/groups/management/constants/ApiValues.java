package com.skillabb.groups.management.constants;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * Clase que con los valores externalizados del o las API's.
 * 
 * @author Jesus Cruz, Skillab.
 */

@Component
@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ApiValues {

  /**
   * Constante externalizada que contiene el paquete base del proyecto.
   */
  @Value("${constants.swagger.basePackage}")
  private String basePackage;

  /**
   * Constante externalizada que contiene el titulo del API.
   */
  @Value("${constants.swagger.title}")
  private String title;

  /**
   * Constante externalizada que contiene la descripcion de la API.
   */
  @Value("${constants.swagger.descriptionApi}")
  private String descriptionApi;

  /**
   * Constante externalizada que contiene la version del swagger.
   */
  @Value("${constants.swagger.version}")
  private String version;

  /**
   * Constante externalizada que contiene el nombre del desarrollador.
   */
  @Value("${constants.swagger.nameDeveloper}")
  private String nameDeveloper;

  /**
   * Constante externalizada que contiene la URL de la peticion.
   */
  @Value("${constants.swagger.contactUrl}")
  private String contactUrl;

  /**
   * Constante externalizada que contiene el mail del desarrollador.
   */
  @Value("${constants.swagger.emailDeveloper}")
  private String emailDeveloper;

  /**
   * Constante externalizada que contiene las etiquetas del swagger.
   */
  @Value("${constants.swagger.label}")
  private String label;

  /**
   * Constante externalizada que contiene el recurso a consumir.
   */
  @Value("${constants.swagger.resourceLocation}")
  private String resourceLocation;

  /**
   * Constante externalizada que contiene donde se generan los artefactos de swagger.
   */
  @Value("${constants.swagger.webjars}")
  private String webjars;

  /**
   * Constante externalizada que contiene la ubicacion de los artefactos.
   */
  @Value("${constants.swagger.webjarsLocation}")
  private String webjarsLocation;

}
