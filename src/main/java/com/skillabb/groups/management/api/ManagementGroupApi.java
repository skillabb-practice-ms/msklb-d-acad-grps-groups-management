package com.skillabb.groups.management.api;

import com.skillabb.groups.management.constants.Constants;
import com.skillabb.groups.management.domain.GroupEntity;
import com.skillabb.groups.management.model.CreateGroupRequest;
import com.skillabb.groups.management.model.DeleteGroupRequest;
import com.skillabb.groups.management.model.UpdateGroupRequest;
import com.skillabb.groups.management.service.ManagementGroupService;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import java.io.Serializable;
import javax.validation.Valid;

@RequestMapping(Constants.BASE_PATH)
@RestController
@Slf4j
public class ManagementGroupApi {

  /**
   * Inyeccion del service para la administracion de grupos.
   */
  @Autowired
  ManagementGroupService managementGroupService;

  @ApiOperation(value = Constants.API_DESCRIPTION, consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE, notes = Constants.API_DESCRIPTION)
  @ApiResponses(value = {@ApiResponse(code = Constants.CODE_NO_CONTENT, message = Constants.OK),
      @ApiResponse(code = Constants.CODE_BAD_REQUEST, message = Constants.BAD_REQUEST),
      @ApiResponse(code = Constants.CODE_UNAUTHORIZED, message = Constants.UNAUTHORIZED),
      @ApiResponse(code = Constants.CODE_UNPROCESSABLE_ENTITY,
          message = Constants.UNPROCESSABLE_ENTITY),
      @ApiResponse(code = Constants.CODE_RESOURCE_NOT_FOUND,
          message = Constants.RESOURCE_NOT_FOUND),
      @ApiResponse(code = Constants.CODE_INTERNAL_ERROR, message = Constants.INTERNAL_ERROR),
      @ApiResponse(code = Constants.CODE_TIMEOUT, message = Constants.MESSAGE_TIMEOUT)})

  @ApiImplicitParams({
      @ApiImplicitParam(required = Constants.TRUE, paramType = Constants.HEADER_WORD_CONSTANT,
          name = Constants.ACCEPT),
      @ApiImplicitParam(required = Constants.TRUE, paramType = Constants.HEADER_WORD_CONSTANT,
          name = Constants.CONTENT_TYPE)})

  @PostMapping(value = Constants.GROUP_CREATE_SPECIFIC_PATH,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Serializable> createGroup(
      @Valid @RequestBody(required = true) CreateGroupRequest request) {
    log.info(Constants.START_CREATE);
    managementGroupService.createGroup(request);
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);

  }

  @ApiOperation(value = Constants.API_DESCRIPTION_DELETE,
      consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE,
      notes = Constants.API_DESCRIPTION)
  @ApiResponses(value = {@ApiResponse(code = Constants.CODE_NO_CONTENT, message = Constants.OK),
      @ApiResponse(code = Constants.CODE_BAD_REQUEST, message = Constants.BAD_REQUEST),
      @ApiResponse(code = Constants.CODE_UNAUTHORIZED, message = Constants.UNAUTHORIZED),
      @ApiResponse(code = Constants.CODE_UNPROCESSABLE_ENTITY,
          message = Constants.UNPROCESSABLE_ENTITY),
      @ApiResponse(code = Constants.CODE_RESOURCE_NOT_FOUND,
          message = Constants.RESOURCE_NOT_FOUND),
      @ApiResponse(code = Constants.CODE_INTERNAL_ERROR, message = Constants.INTERNAL_ERROR),
      @ApiResponse(code = Constants.CODE_TIMEOUT, message = Constants.MESSAGE_TIMEOUT)})

  @ApiImplicitParams({
      @ApiImplicitParam(required = Constants.TRUE, paramType = Constants.HEADER_WORD_CONSTANT,
          name = Constants.ACCEPT),
      @ApiImplicitParam(required = Constants.TRUE, paramType = Constants.HEADER_WORD_CONSTANT,
          name = Constants.CONTENT_TYPE)})
  @DeleteMapping(value = Constants.GROUP_DELETE_SPECIFIC_PATH,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Serializable> deleteGroup(
      @Valid @RequestBody(required = true) DeleteGroupRequest request) {
    log.info(Constants.START_DELET);
    managementGroupService.deleteGroup(request.getGroupId());
    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }



  @ApiOperation(value = Constants.API_DESCRIPTION_READ, consumes = MediaType.APPLICATION_JSON_VALUE,
      produces = MediaType.APPLICATION_JSON_VALUE, notes = Constants.API_DESCRIPTION)
  @ApiResponses(value = {@ApiResponse(code = Constants.CODE_NO_CONTENT, message = Constants.OK),
      @ApiResponse(code = Constants.CODE_BAD_REQUEST, message = Constants.BAD_REQUEST),
      @ApiResponse(code = Constants.CODE_UNAUTHORIZED, message = Constants.UNAUTHORIZED),
      @ApiResponse(code = Constants.CODE_UNPROCESSABLE_ENTITY,
          message = Constants.UNPROCESSABLE_ENTITY),
      @ApiResponse(code = Constants.CODE_RESOURCE_NOT_FOUND,
          message = Constants.RESOURCE_NOT_FOUND),
      @ApiResponse(code = Constants.CODE_INTERNAL_ERROR, message = Constants.INTERNAL_ERROR),
      @ApiResponse(code = Constants.CODE_TIMEOUT, message = Constants.MESSAGE_TIMEOUT)})

  @ApiImplicitParams({
      @ApiImplicitParam(required = Constants.TRUE, paramType = Constants.HEADER_WORD_CONSTANT,
          name = Constants.ACCEPT),
      @ApiImplicitParam(required = Constants.TRUE, paramType = Constants.HEADER_WORD_CONSTANT,
          name = Constants.CONTENT_TYPE)})
  @PostMapping(value = Constants.GROUP_READ_SPECIFIC_PATH,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<GroupEntity> readGroup(
      @Valid @RequestBody(required = true) DeleteGroupRequest request) {
    log.info(Constants.START_READ);
    return new ResponseEntity<GroupEntity>(managementGroupService.readGroup(request.getGroupId()),
        HttpStatus.OK);
  }

  @ApiOperation(value = Constants.API_DESCRIPTION_UPDATE,
      consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE,
      notes = Constants.API_DESCRIPTION)
  @ApiResponses(value = {@ApiResponse(code = Constants.CODE_NO_CONTENT, message = Constants.OK),
      @ApiResponse(code = Constants.CODE_BAD_REQUEST, message = Constants.BAD_REQUEST),
      @ApiResponse(code = Constants.CODE_UNAUTHORIZED, message = Constants.UNAUTHORIZED),
      @ApiResponse(code = Constants.CODE_UNPROCESSABLE_ENTITY,
          message = Constants.UNPROCESSABLE_ENTITY),
      @ApiResponse(code = Constants.CODE_RESOURCE_NOT_FOUND,
          message = Constants.RESOURCE_NOT_FOUND),
      @ApiResponse(code = Constants.CODE_INTERNAL_ERROR, message = Constants.INTERNAL_ERROR),
      @ApiResponse(code = Constants.CODE_TIMEOUT, message = Constants.MESSAGE_TIMEOUT)})

  @ApiImplicitParams({
      @ApiImplicitParam(required = Constants.TRUE, paramType = Constants.HEADER_WORD_CONSTANT,
          name = Constants.ACCEPT),
      @ApiImplicitParam(required = Constants.TRUE, paramType = Constants.HEADER_WORD_CONSTANT,
          name = Constants.CONTENT_TYPE)})
  @PutMapping(value = Constants.GROUP_UPDATE_SPECIFIC_PATH,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  public ResponseEntity<Serializable> updateGroup(
      @Valid @RequestBody(required = true) UpdateGroupRequest request) {
    log.info(Constants.START_UPDATE);

    managementGroupService.updateGroup(request);

    return new ResponseEntity<>(HttpStatus.NO_CONTENT);
  }
}
