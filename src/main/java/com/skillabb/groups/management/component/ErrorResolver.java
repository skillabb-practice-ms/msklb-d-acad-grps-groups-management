package com.skillabb.groups.management.component;

import com.skillabb.groups.management.constants.Constants;
import com.skillabb.groups.management.constants.SpecialCharacterConstants;
import com.skillabb.groups.management.exceptions.ErrorResponse;
import com.skillabb.groups.management.exceptions.ErrorType;
import com.skillabb.groups.management.exceptions.custom.GroupNotExists;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@RestControllerAdvice
@Slf4j
public class ErrorResolver {

  /**
   * Metodo para manejar una excepcion de tipo GroupNotExists.
   *
   * @param req Objeto Http Servlet de petición.
   * @param resp Objeto Http Servlet de respuesta.
   * @param ex Excepción recibida GroupNotExists.
   * @return ErrorResponse Objeto de respuesta específica para GroupNotExists.
   */
  @ExceptionHandler(GroupNotExists.class)
  @ResponseStatus(value = HttpStatus.NOT_FOUND)
  public ErrorResponse resolveDownstreamException(HttpServletRequest req, HttpServletResponse resp,
      GroupNotExists ex) {

    ErrorResponse errorResponse = new ErrorResponse();

    errorResponse.setType(ErrorType.ERROR.name());
    errorResponse.setCode(String.valueOf(HttpStatus.NOT_FOUND.value()));
    errorResponse.setDetails(ex.getMessage());
    errorResponse.setLocation(req.getRequestURI());
    errorResponse.setUuid(req.getHeader(Constants.UUID));
    errorResponse.setTimestamp(ZonedDateTime.now());

    ErrorResolver.writeToLog(errorResponse, ex);

    return errorResponse;

  }


  /**
   * Metodo para manejar una excepcion de tipo {@link HttpRequestMethodNotSupportedException}.
   *
   * @param req Objeto Http Servlet de petición.
   * @param ex Excepción recibida UnauthorizedException.
   * @return errorResponse Objeto de respuesta específica para
   *         HttpRequestMethodNotSupportedException.
   */
  @ExceptionHandler(HttpRequestMethodNotSupportedException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ErrorResponse resolveHttpRequestMethodNotSupportedException(HttpServletRequest req,
      HttpRequestMethodNotSupportedException ex) {

    ErrorResponse errorResponse = new ErrorResponse();

    errorResponse.setType(ErrorType.INVALID.name());
    errorResponse.setCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
    errorResponse.setDetails(ex.getMessage());
    errorResponse.setLocation(req.getRequestURI());
    errorResponse.setTimestamp(ZonedDateTime.now());
    errorResponse.setUuid(req.getHeader(Constants.UUID));

    ErrorResolver.writeToLog(errorResponse, ex);

    return errorResponse;

  }

  /**
   * Metodo para manejar una excepcion de tipo {@link HttpMediaTypeNotAcceptableException}.
   *
   * @param req Objeto Http Servlet de petición.
   * @param ex Excepción recibida HttpMediaTypeNotAcceptableException.
   * @return errorResponse Objeto de respuesta específica para HttpMediaTypeNotAcceptableException.
   */
  @ExceptionHandler(HttpMediaTypeNotAcceptableException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ErrorResponse resolveHttpMediaTypeNotAcceptableException(HttpServletRequest req,
      HttpMediaTypeNotAcceptableException ex) {

    ErrorResponse errorResponse = new ErrorResponse();

    errorResponse.setType(ErrorType.INVALID.name());
    errorResponse.setCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
    errorResponse.setDetails(ex.getMessage());
    errorResponse.setLocation(req.getRequestURI());
    errorResponse.setTimestamp(ZonedDateTime.now());
    errorResponse.setUuid(req.getHeader(Constants.UUID));

    ErrorResolver.writeToLog(errorResponse, ex);

    return errorResponse;

  }

  /**
   * Metodo para manejar una excepcion de tipo {@link HttpMediaTypeNotSupportedException}.
   *
   * @param req Objeto Http Servlet de petición.
   * @param ex Excepción recibida HttpMediaTypeNotSupportedException.
   * @return errorResponse Objeto de respuesta específica para HttpMediaTypeNotSupportedException.
   */
  @ExceptionHandler(HttpMediaTypeNotSupportedException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ErrorResponse resolveHttpMediaTypeNotSupportedException(HttpServletRequest req,
      HttpMediaTypeNotSupportedException ex) {

    ErrorResponse errorResponse = new ErrorResponse();

    errorResponse.setType(ErrorType.INVALID.name());
    errorResponse.setCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
    errorResponse.setDetails(ex.getMessage());
    errorResponse.setLocation(req.getRequestURI());
    errorResponse.setTimestamp(ZonedDateTime.now());
    errorResponse.setUuid(req.getHeader(Constants.UUID));

    ErrorResolver.writeToLog(errorResponse, ex);

    return errorResponse;

  }

  /**
   * Metodo para manejar una excepcion de tipo {@link MethodArgumentNotValidException}.
   *
   * @param req Objeto Http Servlet de petición.
   * @param ex Excepción recibida MethodArgumentNotValidException.
   * @return errorResponse Objeto de respuesta específica para MethodArgumentNotValidException.
   */
  @ExceptionHandler(MethodArgumentNotValidException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ErrorResponse resolveMethodArgumentNotValidException(HttpServletRequest req,
      MethodArgumentNotValidException ex) {

    ErrorResponse errorResponse = new ErrorResponse();

    errorResponse.setType(ErrorType.INVALID.name());
    errorResponse.setCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));

    Map<String, List<String>> groupedErrors = new HashMap<>();

    List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
    List<String> fields = new ArrayList<>();

    for (FieldError fieldError : fieldErrors) {
      String message = fieldError.getDefaultMessage();
      String field = fieldError.getField();
      groupedErrors.computeIfAbsent(message, key -> Collections.singletonList(field));
      fields.add(field);
    }

    if (!groupedErrors.isEmpty()) {
      errorResponse.setDetails(groupedErrors.toString());
    }

    errorResponse.setLocation(req.getRequestURI());
    errorResponse.setMoreInfo(fields.toString());
    errorResponse.setTimestamp(ZonedDateTime.now());
    errorResponse.setUuid(req.getHeader(Constants.UUID));

    ErrorResolver.writeToLog(errorResponse, ex);

    return errorResponse;

  }

  /**
   * Metodo para manejar una excepcion de tipo {@link ServletRequestBindingException}.
   *
   * @param req Objeto Http Servlet de petición.
   * @param ex Excepción recibida ServletRequestBindingException.
   * @return errorResponse Objeto de respuesta específica para ServletRequestBindingException.
   */
  @ExceptionHandler(ServletRequestBindingException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ErrorResponse resolveServletRequestBindingException(HttpServletRequest req,
      ServletRequestBindingException ex) {

    ErrorResponse errorResponse = new ErrorResponse();

    errorResponse.setType(ErrorType.ERROR.name());
    errorResponse.setCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
    errorResponse.setDetails(ex.getMessage());
    errorResponse.setLocation(req.getRequestURI());
    errorResponse.setTimestamp(ZonedDateTime.now());
    errorResponse.setUuid(req.getHeader(Constants.UUID));

    ErrorResolver.writeToLog(errorResponse, ex);

    return errorResponse;

  }

  /**
   * Metodo para manejar una excepcion de tipo {@link Exception}.
   *
   * @param req Objeto Http Servlet de petición.
   * @param ex Excepción recibida Exception.
   * @return errorResponse Objeto de respuesta específica para Exception.
   */
  @ExceptionHandler(Exception.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ErrorResponse resolveException(HttpServletRequest req, Exception ex) {

    ErrorResponse errorResponse = new ErrorResponse();

    errorResponse.setType(ErrorType.FATAL.name());
    errorResponse.setCode(String.valueOf(HttpStatus.INTERNAL_SERVER_ERROR.value()));
    errorResponse.setDetails(ex.getMessage());
    errorResponse.setLocation(req.getRequestURI());
    errorResponse.setTimestamp(ZonedDateTime.now());
    errorResponse.setUuid(req.getHeader(Constants.UUID));


    ErrorResolver.writeToLog(errorResponse, ex);

    return errorResponse;

  }


  /**
   * Metodo para manejar una excepcion de tipo {@link HttpMessageNotReadableException}.
   *
   * @param req Objeto Http Servlet de petición.
   * @param ex Excepción recibida HttpMessageNotReadableException.
   * @return errorResponse Objeto de respuesta específica para HttpMessageNotReadableException.
   */
  @ExceptionHandler(HttpMessageNotReadableException.class)
  @ResponseStatus(value = HttpStatus.BAD_REQUEST)
  public ErrorResponse resolveHttpMessageNotReadableException(HttpServletRequest req,
      HttpMessageNotReadableException ex) {

    ErrorResponse errorResponse = new ErrorResponse();

    String message = Objects.isNull(ex) ? StringUtils.EMPTY : ex.getMessage();
    message = Objects.isNull(message) ? StringUtils.EMPTY : message;
    int index = message.indexOf(SpecialCharacterConstants.COLON);
    message = (index != SpecialCharacterConstants.INT_NEGATIVE_ONE)
        ? message.substring(SpecialCharacterConstants.INT_ZERO_VALUE, index)
        : String.valueOf(HttpStatus.BAD_REQUEST.value());

    errorResponse.setType(ErrorType.INVALID.name());
    errorResponse.setCode(String.valueOf(HttpStatus.BAD_REQUEST.value()));
    errorResponse.setDetails(message);
    errorResponse.setLocation(req.getRequestURI());
    errorResponse.setTimestamp(ZonedDateTime.now());
    errorResponse.setUuid(req.getHeader(Constants.UUID));

    ErrorResolver.writeToLog(errorResponse, ex);

    return errorResponse;

  }

  private static void writeToLog(ErrorResponse errorResponse, Exception exception) {

    log.error(Constants.ERROR_TYPE, errorResponse.getType());
    log.error(Constants.ERROR_CODE, errorResponse.getCode());
    log.error(Constants.ERROR_DETAILS, errorResponse.getDetails());
    log.error(Constants.ERROR_LOCATION, errorResponse.getLocation());
    log.error(Constants.ERROR_MORE_INFORMATION, errorResponse.getMoreInfo());

    String message = Objects.isNull(exception) ? StringUtils.EMPTY : exception.getMessage();

    log.error(message, exception);

  }

}
