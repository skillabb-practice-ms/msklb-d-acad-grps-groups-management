package com.skillabb.groups.management.service;

import com.skillabb.groups.management.domain.GroupEntity;
import com.skillabb.groups.management.model.CreateGroupRequest;
import com.skillabb.groups.management.model.UpdateGroupRequest;

/**
 * Interface que define expone los servicios para crear, actualizar, leer y eliminar grupos.
 * 
 * @author Jesus Cruz.
 */
public interface ManagementGroupService {

  /**
   * Metodo que realiza la creacion de un grupo.
   * 
   * @param request del tipo ManagementGroupRequest.
   */
  public abstract void createGroup(CreateGroupRequest request);

  /**
   * Metodo que realiza la actualizacion de un grupo.
   * 
   * @param request del tipo ManagementGroupRequest.
   */
  public abstract void updateGroup(UpdateGroupRequest request);

  /**
   * Metodo que realiza la consulta de un grupo.
   * 
   * @param studentId GroupId.
   * @return
   */
  public abstract GroupEntity readGroup(int studentId);

  /**
   * Metodo que realiza la eliminacion de un grupo.
   * 
   * @param studentId GroupId.
   */
  public abstract void deleteGroup(int studentId);
}
