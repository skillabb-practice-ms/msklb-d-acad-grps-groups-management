package com.skillabb.groups.management.config;

import com.skillabb.groups.management.constants.ApiValues;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * Clase de configuración para el contexto web mvc.
 * 
 * @author Jesus Cruz.
 */
@Configuration
public class WebMvcConfig implements WebMvcConfigurer {

  /**
   * Inyeccion de las contantes externalizadas de la aplicacion.
   */
  @Autowired
  private ApiValues apiValues;


  /**
   * Metodo para agregar recursos estaticos.
   * 
   * @param registry objeto que registra los recursos.
   */
  // @Override
  // public void addResourceHandlers(ResourceHandlerRegistry registry) {
  //
  // registry.addResourceHandler("swagger-ui.html")
  // .addResourceLocations("classpath:/META-INF/resources/");
  //
  // registry.addResourceHandler("/webjars/**")
  // .addResourceLocations("classpath:/META-INF/resources/webjars/");
  //
  // WebMvcConfigurer.super.addResourceHandlers(registry);
  //
  // }

  /**
   * Metodo para agregar recursos estaticos.
   * 
   * @param registry objeto que registra los recursos.
   */
  @Override
  public void addResourceHandlers(ResourceHandlerRegistry registry) {

    registry.addResourceHandler(apiValues.getLabel())
        .addResourceLocations(apiValues.getResourceLocation());

    registry.addResourceHandler(apiValues.getWebjars())
        .addResourceLocations(apiValues.getWebjarsLocation());

    WebMvcConfigurer.super.addResourceHandlers(registry);

  }



}
