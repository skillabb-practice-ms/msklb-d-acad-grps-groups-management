package com.skillabb.groups.management.config;

import com.skillabb.groups.management.constants.EntityConstants;
import com.skillabb.groups.management.domain.GroupEntity;
import com.skillabb.groups.management.repository.GroupsRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * Clase que ejecuta un runner para insertar datos en la db al arranque.
 * 
 * @author JesúsCruzHernández
 *
 */
@Configuration
public class GroupsConfig {

  @Bean
  CommandLineRunner commandLineRunner(GroupsRepository repository) {

    return args -> {
      GroupEntity group1 = GroupEntity.builder().name(EntityConstants.GROUP_NAME)
          .status(EntityConstants.GROUP_STATUS).abbreviation(EntityConstants.GROUP_ABBREVIATION)
          .status2(EntityConstants.GROUP_STATUS_DOS).code(EntityConstants.GROUP_CODE)
          .generation(EntityConstants.GROUP_GENERATION).createdAt(LocalDateTime.now())
          .lastModifiedAt(LocalDateTime.now()).build();
      repository.saveAll(Arrays.asList(group1));
    };
  }
}
