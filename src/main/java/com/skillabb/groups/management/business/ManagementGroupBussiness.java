package com.skillabb.groups.management.business;

import com.skillabb.groups.management.constants.Constants;
import com.skillabb.groups.management.domain.GroupEntity;
import com.skillabb.groups.management.exceptions.custom.GroupNotExists;
import com.skillabb.groups.management.model.CreateGroupRequest;
import com.skillabb.groups.management.model.UpdateGroupRequest;
import com.skillabb.groups.management.repository.GroupsRepository;
import com.skillabb.groups.management.service.ManagementGroupService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.time.LocalDateTime;
import javax.transaction.Transactional;

/**
 * Capa bussiness que implementa los metodos para la administracion de grupos.
 * 
 * @author Jesus Cruz.
 */
@Service
public class ManagementGroupBussiness implements ManagementGroupService {


  /**
   * Inyeccion del repository de grupos.
   */
  @Autowired
  private GroupsRepository groupsRepository;

  @Override
  public void createGroup(CreateGroupRequest request) {

    GroupEntity entity = GroupEntity.builder().build();
    BeanUtils.copyProperties(request, entity);
    entity.setCreatedAt(LocalDateTime.now());
    entity.setLastModifiedAt(LocalDateTime.now());
    groupsRepository.save(entity);
  }


  /**
   * Metodo que se encarga de actualizar un usuario.
   */
  @Override
  @Transactional
  public void updateGroup(UpdateGroupRequest request) {

    GroupEntity group = groupsRepository.findById(request.getGroupId())
        .orElseThrow(() -> new GroupNotExists(new StringBuilder().append(Constants.MESSAGE_1)
            .append(request.getGroupId()).append(Constants.MESSAGE_2).toString()));

    BeanUtils.copyProperties(request, group);
    group.setLastModifiedAt(LocalDateTime.now());

  }

  /**
   * Metodo que se encarga de obtener la informacion de un grupo.
   */
  @Override
  public GroupEntity readGroup(int groupId) {

    GroupEntity group =
        groupsRepository.findById(groupId).orElseThrow(() -> new GroupNotExists(new StringBuilder()
            .append(Constants.MESSAGE_1).append(groupId).append(Constants.MESSAGE_2).toString()));

    return group;
  }

  /**
   * Medoto que se encarga de eliminar un grupo.
   */
  @Override
  public void deleteGroup(int groupId) {

    if (!groupsRepository.existsById(groupId)) {
      throw new GroupNotExists(new StringBuilder().append(Constants.MESSAGE_1).append(groupId)
          .append(Constants.MESSAGE_2).toString());
    }

    groupsRepository.deleteById(groupId);

  }

}
