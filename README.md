# CRUD-GROUPS

Este microservicio tiene 4 API's que se encargan de realizar lo siguiente:

1.API que permite crear un grupo.

### RECURSO

PATH:

> /v1.1/skillabb/practice/group/create  POST

URL:

> http://localhost:8080

Test data: 
 
``` json
{
  "abbreviation": "gprD",
  "code": "00004",
  "generation": "2022",
  "name": "Grupo D",
  "status": 1,
  "status2": 1
}
```

2.API que permite actualizar un grupo.

### RECURSO

PATH:

> /v1.1/skillabb/practice/group/update  PUT

URL:

> http://localhost:8080

Test data: 
 
``` json
{
  "groupId" : 1,
  "abbreviation": "gprD",
  "code": "00007",
  "generation": "2022",
  "name": "Grupo F",
  "status": 1,
  "status2": 1
}
```

3.API que permite consultar un grupo.

### RECURSO

PATH:

> /v1.1/skillabb/practice/group/read  POST

URL:

> http://localhost:8080

Test data: 
 
``` json
{
  "groupId": 1
}
```

4.API que permite eliminar un grupo.

### RECURSO

PATH:

> /v1.1/skillabb/practice/group/delete  DELETE

URL:

> http://localhost:8080

Test data: 
 
``` json
{
  "groupId": 1
}
```


### Tools

* Maven
* Spring
* SpringBoot
* Spring Tools Suite
* Lombok

### Prerequisitos

Se necesita tener instalado:
		
- Java 1.8  		
- Maven 		
- Spring Tools Suite
- lombok
 

### Deploy
    mvn spring boot:run  en Local 
   
O desde Spring Tools Suite click derecho sobre el proyecto -> Run As -> Spring Boot App
