FROM  openjdk:8-jdk-alpine

LABEL version="1.0.0"
LABEL description="Groups manager service for skillabb ms practice"
LABEL copyright="skillabb.com"

RUN apk --no-cache add curl

ARG JAR_FILE=target/groups-management.jar

ADD ${JAR_FILE} /app/groups-management.jar

CMD ["java", "-Xmx200m", "-jar", "/app/groups-management.jar"]
